module.exports = {

    tags: ['login'],
    
    'Test Case-1:': (browser) => {
            browser.windowMaximize()
            const page = browser.page.xpath();
            page.navigate()
            .checkJoyalukkasPage()
            .saveScreenshot('test/screenshots/' + 'jos-main-page.png')
    },

    'Test Case-2:': (browser) => {
        const page = browser.page.xpath();
        page
            .userLogin()
    },

    'Test Case-3:' : (browser) => {
        const page = browser.page.xpath();
        page
        .inValidatingUsernamePassword()
        .loginErrorMessage()
        .saveScreenshot('test/screenshots/' + 'jos-fail.png')
        
    },
    'Test Case-4: ': (browser) => {
        const page = browser.page.xpath();
      
       page
           .validatingUsernamePassword()
           .saveScreenshot('test/screenshots/' + 'jos-success.png')
    },
    'Test Case-5:' : (browser) => {
        const page = browser.page.xpath();
        page
        .SelectingCategory()
    },
    'Test Case-6:' : (browser) => {
        const page = browser.page.xpath();
        page
        .Filtering()
       
    },
    'Test Case-7:' : (browser) => {
        const page = browser.page.xpath();
        var prices = new Array();
        page
        .FilteringPrice()
        .PriceSelect()
        .pause(15000)
        browser.elements('css selector', '.sp_amt', elements => {
           elements.value.forEach(element =>{
           let key = Object.keys(element)[0];
           let ElementIDvalue = element[key];
          

           browser.elementIdText(ElementIDvalue, function(result) {
               var filtered= result.value.replace(',', '');
               prices.push(filtered);
               
               var lengthOfArray = prices.length;
               var currentPrice = prices[0];
               var nextPrice = prices[lengthOfArray-1];
               if(currentPrice <= nextPrice){
                   console.log("Sorted Properly");
               }else{
                   console.log("Not Sorted Properly");
               }
           })
           })
           });
        browser.pause(5000)
        // browser.end()      
    },
    'Test Case-8' : (browser) => {
        const page = browser.page.xpath();
        page
        .Myacnt()
        .InMyacnt()
        .Address()
        browser.pause(5000)
        browser.end()
    }
} 
